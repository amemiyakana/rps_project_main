$(function() {
	$('#siteSearch .button').hover(
		function() {
			$(this).addClass('buttonHover');
		},
		function() {
			$(this).removeClass('buttonHover');
		}
	);
	$('#searchKeywordSearch .searchButton').hover(
		function() {
			$(this).addClass('searchButtonHover');
		},
		function() {
			$(this).removeClass('searchButtonHover');
		}
	);
	$('#sitemapLink').hover(
		function() {
			$(this).addClass('sitemapLinkHover');
		},
		function() {
			$(this).removeClass('sitemapLinkHover');
		}
	);
});

var uai = new UAIdentifier();
document.write("<style type=\"text/css\">body{font-family:");

if (uai.mac)
	document.write("\"ヒラギノ角ゴ Pro W3\",\"Hiragino Kaku Gothic Pro\",Osaka,sans-serif");
 else
	document.write("\"メイリオ\",Meiryo,\"ＭＳ Ｐゴシック\",\"MS PGothic\",sans-serif");

document.write("}</style>");

window.onload = function(e) {
	var cookie = readCookie("style");
	
	if (cookie)
		var title = cookie;
	 else
		var title = getPreferredStyleSheet();
	
	setActiveStyleSheet(title);
}

window.onunload = function(e) {
	var title = getActiveStyleSheet();
	createCookie("style", title, 365);
}

var cookie = readCookie("style");

if (cookie)
	var title = cookie;
 else
	var title = getPreferredStyleSheet();

setActiveStyleSheet(title);

function UAIdentifier() {
	if (typeof(navigator) != "object" || !navigator.userAgent) {
		this.unknown = true;
		return;
	}
	
	var ua = navigator.userAgent;
	var match;
	
	if (typeof(RegExp) == "undefined") {
		if (ua.indexOf("Opera") >= 0)
			this.opera = true;
		 else if (ua.indexOf("Netscape") >= 0)
			this.netscape = true;
		 else if (ua.indexOf("Mozilla/") == 0)
			this.mozilla = true;
		 else
			this.unknown = true;
		
		if (ua.indexOf("Gecko/") >= 0)
			this.gecko = true;
		 else if (ua.indexOf("Presto/") >= 0)
			this.presto = true;
		
		if (ua.indexOf("Win") >= 0)
			this.windows = true;
		 else if (ua.indexOf("Mac") >= 0)
			this.mac = true;
		 else if (ua.indexOf("Linux") >= 0)
			this.linux = true;
		 else if (ua.indexOf("BSD") >= 0)
			this.bsd = true;
		 else if (ua.indexOf("SunOS") >= 0)
			this.sunos = true;
		
		return;
	}
	
	/*@cc_on
	@if (@_jscript)
		function jscriptVersion() {
			switch (@_jscript_version) {
				case 3.0:
					return "4.0";
				
				case 5.0:
					return "5.0";
				
				case 5.1:
					return "5.01";
				
				case 5.5:
					return "5.5";
				
				case 5.6:
					if ("XMLHttpRequest" in window)
						return "7.0";
					
					return "6.0";
				
				case 5.7:
					return "7.0";
				
				case 5.8:
					return "8.0";
				
				default:
					return true;
			}
		}
		
		if (@_win16 || @_win32 || @_win64) {
			this.windows = true;
			this.trident = jscriptVersion();
		} else if (@_mac || navigator.platform.indexOf("Mac") >= 0) {
			this.mac = true;
			this.tasman = jscriptVersion();
		}
		
		if (match = ua.match("MSIE ?(\\d+\\.\\d+)b?;")) {
			this.ie = match[1];
		}
	@else @*/
	
	if (match = ua.match("AppleWebKit/(\\d+(\\.\\d+)*)"))
		this.applewebkit = match[1];
	 else if (typeof(Components) == "object") {
		if (match = ua.match("Gecko/(\\d{8})"))
		 	this.gecko = match[1];
		 else if (navigator.product == "Gecko" && (match = navigator.productSub.match("^(\\d{8})$")))
			this.gecko = match[1];
	} else if (typeof(opera) == "object" && (match = ua.match("Presto/(\\d+(\\.\\d+)*)")))
		this.presto = match[1];
	/*@end @*/
	
	if (typeof(opera) == "object" && typeof(opera.version) == "function")
		this.opera = opera.version();
	 else if (typeof(opera) == "object" && (match = ua.match("Opera[/ ](\\d+\\.\\d+)")))
		this.opera = match[1];
	 else if (this.ie) {}
	 else if (match = ua.match("Epiphany/(\\d+(\\.\\d+)*)"))
		this.epiphany = match[1];
	 else if (match = ua.match("Chrome/(\\d+(\\.\\d+)*)"))
		this.chrome = match[1];
	 else if (match = ua.match("Safari/(\\d+(\\.\\d+)*)"))
		this.safari = match[1];
	 else if (match = ua.match("Konqueror/(\\d+(\\.\\d+)*)"))
		this.konqueror = match[1];
	 else if (ua.indexOf("(compatible;") < 0 && (match = ua.match("^Mozilla/(\\d+\\.\\d+)"))) {
		this.mozilla = match[1];
		
		if (match = ua.match("\\([^(]*rv:(\\d+(\\.\\d+)*).*?\\)"))
			this.mozillarv = match[1];
		
		if (match = ua.match("Firefox/(\\d+(\\.\\d+)*)"))
			this.firefox = match[1];
		 else if (match = ua.match("Netscape\\d?/(\\d+(\\.\\d+)*)"))
			this.netscape = match[1];
	} else
		this.unknown = true;
	
	if (ua.indexOf("Win 9x 4.90") >= 0)
		this.windows = "ME";
	 else if (match = ua.match("Win(dows)? ?(NT ?(\\d+\\.\\d+)?|\\d+|XP|ME|Vista)")) {
		this.windows = match[2];
		
		if (match[3])
			this.winnt = match[3];
		 else switch (match[2]) {
			case "2000":
				this.winnt = "5.0";
				break;
			
			case "XP":
				this.winnt = "5.1";
				break;
			
			case "Vista":
				this.winnt = "6.0";
				break;
		}
	} else if (ua.indexOf("Mac") >= 0)
		this.mac = true;
	 else if (ua.indexOf("Linux") >= 0)
		this.linux = true;
	 else if (match = ua.match("\\w*BSD"))
		this.bsd = match[0];
	 else if (ua.indexOf("SunOS") >= 0)
		this.sunos = true;
	
	if (this.safari && !this.windows && !this.mac) {
		delete this.safari;
		this.unknown = true;
	}
}

UAIdentifier.prototype.toString = function() {
	var r = "";
	
	if (this.opera) {
		r += "Opera";
		
		if (this.opera !== true)
			r += ":" + this.opera;
	} else if (this.ie) {
		r += "IE";
		
		if (this.ie !== true)
			r += ":" + this.ie;
	} else if (this.chrome)
		r += "Chrome:" + this.chrome;
	 else if (this.safari)
		r += "Safari:" + this.safari;
	 else if (this.konqueror)
		r += "Konqueror:" + this.konqueror;
	 else if (this.mozilla) {
		r += "Mozilla";
		
		if (this.mozilla !== true) {
			r += ":" + this.mozilla;
			
			if (this.mozillarv)
				r += ":" + this.mozillarv;
		}
		
		if (this.firefox)
			r += ",Firefox:" + this.firefox;
		 else if (this.netscape)
			r += ",Netscape:" + this.netscape;
	} else
		r += "Unknown";
	
	if (this.trident) {
		r += ",Trident";
		
		if (this.iec !== true)
			r += ":" + this.trident;
	} else if (this.tasman) {
		r += ",Tasman";
		
		if (this.iec !== true)
			r += ":" + this.tasman;
	} else if (this.gecko) {
		r += ",Gecko";
		
		if (this.gecko !== true)
			r += ":" + this.gecko;
	} else if (this.presto) {
		r += ",Presto";
		
		if (this.preto !== true)
			r += ":" + this.presto;
	} else if (this.applewebkit)
		r += ",AppleWebKit:" + this.applewebkit;
	
	if (this.windows) {
		r += ",Win";
		
		if (this.winnt)
			r += "NT:" + this.winnt;
		 else if (this.windows !== true)
			r += ":" + this.windows;
	} else if (this.mac)
		r += ",Mac";
	 else if (this.linux)
		r += ",Linux";
	 else if (this.bsd)
		r += "," + ((this.bsd === true)? "BSD": this.bsd);
	 else if (this.sunos)
		r += ",Solaris";
	
	return r;
};

/**
 * アクティブなスタイルシートを設定する
 * 
 * @param  string title アクティブにするスタイルシートの link タグに設定されたタイトル
 * @return void
 */
function setActiveStyleSheet(title) {
	var i, a, main;
	
	for (i = 0; (a = document.getElementsByTagName("link")[i]); i++)
		if (a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
			a.disabled = true;
			
			if (a.getAttribute("title") == title)
				a.disabled = false;
		}
}

/**
 * 現在アクティブに設定されているスタイルシートを取得する
 * 
 * @param  void
 * @return string 現在アクティブなスタイルシートの link タグに設定されたタイトル
 */
function getActiveStyleSheet() {
	var i, a;
	
	for (i = 0; (a = document.getElementsByTagName("link")[i]); i++)
		if (a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title") && !a.disabled)
			return a.getAttribute("title");
	
	return null;
}

/**
 * 初期状態のスタイルシートを取得する
 * 
 * @param  void
 * @return string 初期状態のスタイルシートの link タグに設定されたタイトル
 */
function getPreferredStyleSheet() {
	var i, a;
	
	for (i = 0; (a = document.getElementsByTagName("link")[i]); i++)
		if (a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("rel").indexOf("alt") == -1 && a.getAttribute("title"))
			return a.getAttribute("title");
	
	return null;
}

/**
 * スタイルシートに関するcookieを作成
 * 
 * @param  string name  cookieに記録する情報の名前
 * @param  string value cookieに記録する値
 * @param  int    days  cookieの有効日数
 * @return void
 */
function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else
		expires = "";
	
	document.cookie = name + "=" + value + expires + "; path=/";
}

/**
 * cookieを取得する
 * 
 * @param  string name 取得するcookieの名前
 * @return string 設定するスタイルシートの名前
 */
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	
	return null;
}

function changeItemLargePhoto(num) {
	var target = document.getElementById("itemPhotoLargeImage");
	target.src = itemLargePhotoList[num];
}
