/*resize*/
$(function() {
	var w = $('body').width();
	var x = 950;
	$(resize);
	$(window).resize(function() {
		w = $('body').width();
		$(resize);
	});
	function resize() {
		if(w<=x) {
			$('#main').css('width',950);
		} else {
			$('#main').css({'width':''});
		}
	}
});



/*modal*/
$(function() {
	var time = 500;
	$('.modal_open').click(function() {
		$('#modal').fadeIn(time);
		$('#modal_ov').fadeIn(time);
	});
	$('.modal_close, #modal_ov').click(function() {
		$('#modal').fadeOut(time);
		$('#modal_ov').fadeOut(time);
	});
});



/*step01*/
$(function() {
	$(window).load(function() {
		var flg11 = localStorage.getItem('sp_winter2016_1_1');
		var flg12 = localStorage.getItem('sp_winter2016_1_2');
		var flg13 = localStorage.getItem('sp_winter2016_1_3');
		var flg14 = localStorage.getItem('sp_winter2016_1_4');
		var flg15 = localStorage.getItem('sp_winter2016_1_5');
		if(flg11) {
			$('#main.step1 #sec02 ul li.li01').addClass('active');
			$('#main.entry #sec01 ul.ul01 li.li01').show();
		}
		if(flg12) {
			$('#main.step1 #sec02 ul li.li02').addClass('active');
			$('#main.entry #sec01 ul.ul01 li.li02').show();
		}
		if(flg13) {
			$('#main.step1 #sec02 ul li.li03').addClass('active');
			$('#main.entry #sec01 ul.ul01 li.li03').show();
		}
		if(flg14) {
			$('#main.step1 #sec02 ul li.li04').addClass('active');
			$('#main.entry #sec01 ul.ul01 li.li04').show();
		}
		if(flg15) {
			$('#main.step1 #sec02 ul li.li05').addClass('active');
			$('#main.entry #sec01 ul.ul01 li.li05').show();
		}
		if(flg11 && flg12 && flg13 && flg14 && flg15) {
			$('#main.entry #sec01 ul.ul01 li.li06').show();
		}
	});
});



/*step02*/
$(function() {
	$(window).load(function() {
		var flg21 = localStorage.getItem('sp_winter2016_2_1');
		var flg22 = localStorage.getItem('sp_winter2016_2_2');
		var flg23 = localStorage.getItem('sp_winter2016_2_3');
		var flg24 = localStorage.getItem('sp_winter2016_2_4');
		var flg25 = localStorage.getItem('sp_winter2016_2_5');
		if(flg21) {
			$('#main.step2 #sec02 ul li.li01').addClass('active');
			$('#main.entry #sec01 ul.ul02 li.li01').show();
		}
		if(flg22) {
			$('#main.step2 #sec02 ul li.li02').addClass('active');
			$('#main.entry #sec01 ul.ul02 li.li02').show();
		}
		if(flg23) {
			$('#main.step2 #sec02 ul li.li03').addClass('active');
			$('#main.entry #sec01 ul.ul02 li.li03').show();
		}
		if(flg24) {
			$('#main.step2 #sec02 ul li.li04').addClass('active');
			$('#main.entry #sec01 ul.ul02 li.li04').show();
		}
		if(flg25) {
			$('#main.step2 #sec02 ul li.li05').addClass('active');
			$('#main.entry #sec01 ul.ul02 li.li05').show();
		}
		if(flg21 && flg22 && flg23 && flg24 && flg25) {
			$('#main.entry #sec01 ul.ul02 li.li06').show();
		}
	});
});



/*step03*/
$(function() {
	$(window).load(function() {
		var flg31 = localStorage.getItem('sp_winter2016_3_1');
		var flg32 = localStorage.getItem('sp_winter2016_3_2');
		var flg33 = localStorage.getItem('sp_winter2016_3_3');
		var flg34 = localStorage.getItem('sp_winter2016_3_4');
		var flg35 = localStorage.getItem('sp_winter2016_3_5');
		if(flg31) {
			$('#main.step3 #sec02 ul li.li01').addClass('active');
			$('#main.entry #sec01 ul.ul03 li.li01').show();
		}
		if(flg32) {
			$('#main.step3 #sec02 ul li.li02').addClass('active');
			$('#main.entry #sec01 ul.ul03 li.li02').show();
		}
		if(flg33) {
			$('#main.step3 #sec02 ul li.li03').addClass('active');
			$('#main.entry #sec01 ul.ul03 li.li03').show();
		}
		if(flg34) {
			$('#main.step3 #sec02 ul li.li04').addClass('active');
			$('#main.entry #sec01 ul.ul03 li.li04').show();
		}
		if(flg35) {
			$('#main.step3 #sec02 ul li.li05').addClass('active');
			$('#main.entry #sec01 ul.ul03 li.li05').show();
		}
		if(flg31 && flg32 && flg33 && flg34 && flg35) {
			$('#main.entry #sec01 ul.ul03 li.li06').show();
		}
	});
});



/*point*/
$(function() {
		$(window).load(function() {
			var count=0;
			var flg11 = localStorage.getItem('sp_winter2016_1_1');
			var flg12 = localStorage.getItem('sp_winter2016_1_2');
			var flg13 = localStorage.getItem('sp_winter2016_1_3');
			var flg14 = localStorage.getItem('sp_winter2016_1_4');
			var flg15 = localStorage.getItem('sp_winter2016_1_5');
			var flg21 = localStorage.getItem('sp_winter2016_2_1');
			var flg22 = localStorage.getItem('sp_winter2016_2_2');
			var flg23 = localStorage.getItem('sp_winter2016_2_3');
			var flg24 = localStorage.getItem('sp_winter2016_2_4');
			var flg25 = localStorage.getItem('sp_winter2016_2_5');
			var flg31 = localStorage.getItem('sp_winter2016_3_1');
			var flg32 = localStorage.getItem('sp_winter2016_3_2');
			var flg33 = localStorage.getItem('sp_winter2016_3_3');
			var flg34 = localStorage.getItem('sp_winter2016_3_4');
			var flg35 = localStorage.getItem('sp_winter2016_3_5');
			if(flg11) {count = count+1;}
			if(flg12) {count = count+1;}
			if(flg13) {count = count+1;}
			if(flg14) {count = count+1;}
			if(flg15) {count = count+1;}
			if(flg21) {count = count+1;}
			if(flg22) {count = count+1;}
			if(flg23) {count = count+1;}
			if(flg24) {count = count+1;}
			if(flg25) {count = count+1;}
			if(flg31) {count = count+1;}
			if(flg32) {count = count+1;}
			if(flg33) {count = count+1;}
			if(flg34) {count = count+1;}
			if(flg35) {count = count+1;}
			if(count>=1) {
				$('#main.entry #sec02 #num00').show();
				$('#main.entry #sec02 #num01').show().addClass('num01');
			}
			if(count>=2) {
				$('#main.entry #sec02 #num01').removeClass('num01');
				$('#main.entry #sec02 #num01').addClass('num02');
			}
			if(count>=3) {
				$('#main.entry #sec02 #num01').removeClass('num02');
				$('#main.entry #sec02 #num01').addClass('num03');
			}
			if(count>=4) {
				$('#main.entry #sec02 #num01').removeClass('num03');
				$('#main.entry #sec02 #num01').addClass('num04');
			}
			if(count>=5) {
				$('#main.entry #sec02 #num01').removeClass('num04');
				$('#main.entry #sec02 #num01').addClass('num05');
			}
			if(count>=6) {
				$('#main.entry #sec02 #num01').removeClass('num05');
				$('#main.entry #sec02 #num01').addClass('num06');
			}
			if(count>=7) {
				$('#main.entry #sec02 #num01').removeClass('num06');
				$('#main.entry #sec02 #num01').addClass('num07');
			}
			if(count>=8) {
				$('#main.entry #sec02 #num01').removeClass('num07');
				$('#main.entry #sec02 #num01').addClass('num08');
			}
			if(count>=9) {
				$('#main.entry #sec02 #num01').removeClass('num08');
				$('#main.entry #sec02 #num01').addClass('num09');
			}
			if(count>=10) {
				$('#main.entry #sec02 #num02').show();
				$('#main.entry #sec02 #num01').removeClass('num09');
				$('#main.entry #sec02 p.txt01').show();
				$('#main.entry #sec02 ul li.li01').show();
			}
			if(count>=11) {
				$('#main.entry #sec02 #num01').show().addClass('num01');
			}
			if(count>=12) {
				$('#main.entry #sec02 #num01').removeClass('num01');
				$('#main.entry #sec02 #num01').addClass('num02');
			}
			if(count>=13) {
				$('#main.entry #sec02 #num01').removeClass('num02');
				$('#main.entry #sec02 #num01').addClass('num03');
			}
			if(count>=14) {
				$('#main.entry #sec02 #num01').removeClass('num03');
				$('#main.entry #sec02 #num01').addClass('num04');
			}
			if(count>=15) {
				$('#main.entry #sec02 #num01').removeClass('num04');
				$('#main.entry #sec02 #num01').addClass('num05');
				$('#main.entry #sec02 p.txt02').show();
				$('#main.entry #sec02 ul li.li02').show();
			}
		});
});