/*resize*/
$(function() {
	var w = $('body').width();
	var x = 950;
	$(resize);
	$(window).resize(function() {
		w = $('body').width();
		$(resize);
	});
	function resize() {
		if(w<=x) {
			$('#main').css('width',950);
		} else {
			$('#main').css({'width':''});
		}
	}
});



/*campaign01*/
$(function() {
	var flg11 = localStorage.getItem('sp_autumn2016_1_1');
	var flg12 = localStorage.getItem('sp_autumn2016_1_2');
	var flg13 = localStorage.getItem('sp_autumn2016_1_3');
	var flg14 = localStorage.getItem('sp_autumn2016_1_4');
	var flg15 = localStorage.getItem('sp_autumn2016_1_5');
	if(flg11) {
		$('#main #sec01 .cnts01 .block02 ul li.li02').addClass('active');
	}
	if(flg12) {
		$('#main #sec01 .cnts01 .block02 ul li.li01').addClass('active');
	}
	if(flg13) {
		$('#main #sec01 .cnts01 .block02 ul li.li03').addClass('active');
	}
	if(flg14) {
		$('#main #sec01 .cnts01 .block02 ul li.li04').addClass('active');
	}
	if(flg15) {
		$('#main #sec01 .cnts01 .block02 ul li.li05').addClass('active');
	}
	if(flg11&&flg12&&flg13&&flg14&&flg15) {
		$('#main #sec01 .cnts01 .block03').addClass('active');
	}
});

/*campaign02*/
$(function() {
	var flg21 = localStorage.getItem('sp_autumn2016_2_1');
	var flg22 = localStorage.getItem('sp_autumn2016_2_2');
	var flg23 = localStorage.getItem('sp_autumn2016_2_3');
	var flg24 = localStorage.getItem('sp_autumn2016_2_4');
	var flg25 = localStorage.getItem('sp_autumn2016_2_5');
	var flg2b = localStorage.getItem('sp_autumn2016_2_b');
	if(flg21) {
		$('#main #sec01 .cnts02 .block02 ul li.li01').addClass('active');
	}
	if(flg22) {
		$('#main #sec01 .cnts02 .block02 ul li.li02').addClass('active');
	}
	if(flg23) {
		$('#main #sec01 .cnts02 .block02 ul li.li03').addClass('active');
	}
	if(flg24) {
		$('#main #sec01 .cnts02 .block02 ul li.li04').addClass('active');
	}
	if(flg25) {
		$('#main #sec01 .cnts02 .block02 ul li.li05').addClass('active');
	}
	if(flg21&&flg22&&flg23&&flg24&&flg25) {
		$('#main #sec01 .cnts02 .block03 .block03_01').addClass('active');
	}
	if(flg2b) {
		$('#main #sec01 .cnts02 .block03 .block03_02').addClass('active');
	}
});

/*campaign03*/
$(function() {
	var flg31 = localStorage.getItem('sp_autumn2016_3_1');
	var flg32 = localStorage.getItem('sp_autumn2016_3_2');
	var flg33 = localStorage.getItem('sp_autumn2016_3_3');
	var flg34 = localStorage.getItem('sp_autumn2016_3_4');
	var flg35 = localStorage.getItem('sp_autumn2016_3_5');
	var flg3b = localStorage.getItem('sp_autumn2016_3_b');
	if(flg31) {
		$('#main #sec01 .cnts03 .block02 ul li.li01').addClass('active');
	}
	if(flg32) {
		$('#main #sec01 .cnts03 .block02 ul li.li02').addClass('active');
	}
	if(flg33) {
		$('#main #sec01 .cnts03 .block02 ul li.li03').addClass('active');
	}
	if(flg34) {
		$('#main #sec01 .cnts03 .block02 ul li.li04').addClass('active');
	}
	if(flg35) {
		$('#main #sec01 .cnts03 .block02 ul li.li05').addClass('active');
	}
	if(flg31&&flg32&&flg33&&flg34&&flg35) {
		$('#main #sec01 .cnts03 .block03 .block03_01').addClass('active');
	}
	if(flg3b) {
		$('#main #sec01 .cnts03 .block03 .block03_02').addClass('active');
	}
});



/*lesson02*/
$(function() {
	var flg0101,flg0102,flg0103,flg0104,flg0201,flg0202,flg0203,flg0204;
	var flg2b = localStorage.getItem(c2b);
	var c2b = 'sp_autumn2016_2_b';
	$('.lesson2 #sec04 .start img').click(function() {
		$(this).closest('.btn01').hide();
		$(this).closest('.btn01').next('.block').fadeIn().addClass('active');
	});

	$('.lesson2 #sec04 .block ul li').click(function() {
		if(!($(this).hasClass('active'))) {
			$(this).siblings().removeClass('active');
			$(this).addClass('active');
			$(this).find('img').attr("src",$(this).find('img').attr("src").replace('_off', '_on'));
			$(this).siblings().each(function() {
				$(this).find('img').attr("src",$(this).find('img').attr("src").replace('_on', '_off'));
			});
			
			$('.lesson2 #sec04 .block01 ul li').each(function() {
				if($(this).hasClass('active') ) {
					if( $(this).hasClass('li01') ) {flg0101 = 1;}
					else {flg0101 = 2;}
				}
			});
			$('.lesson2 #sec04 .block01_02 ul li').each(function() {
				if($(this).hasClass('active') ) {
					if($(this).hasClass('li01')) {flg0102 = 1;}
					else if($(this).hasClass('li02')) {flg0102 = 2;}
					else {flg0102 = 3;}
				}
			});
			$('.lesson2 #sec04 .block01_03 ul li').each(function() {
				if($(this).hasClass('active') ) {
					if($(this).hasClass('li01')) {flg0103 = 1;}
					else if($(this).hasClass('li02')) {flg0103 = 2;}
					else if($(this).hasClass('li03')) {flg0103 = 3;}
					else {flg0103 = 4;}
				}
			});
			$('.lesson2 #sec04 .block01_04 ul li').each(function() {
				if($(this).hasClass('active') ) {
					if($(this).hasClass('li01')) {flg0104 = 1;}
					else if($(this).hasClass('li02')) {flg0104 = 2;}
					else if($(this).hasClass('li03')) {flg0104 = 3;}
					else {flg0104 = 4;}
				}
			});
			$('.lesson2 #sec04 .block02_02 ul li').each(function() {
				if($(this).hasClass('active') ) {
					if($(this).hasClass('li01')) {flg0202 = 1;}
					else if($(this).hasClass('li02')) {flg0202 = 2;}
					else if($(this).hasClass('li03')) {flg0202 = 3;}
					else {flg0202 = 4;}
				}
			});
			$('.lesson2 #sec04 .block02_03 ul li').each(function() {
				if($(this).hasClass('active') ) {
					if($(this).hasClass('li01')) {flg0203 = 1;}
					else if($(this).hasClass('li02')) {flg0203 = 2;}
					else if($(this).hasClass('li03')) {flg0203 = 3;}
					else {flg0203 = 4;}
				}
			});
			$('.lesson2 #sec04 .block02_04 ul li').each(function() {
				if($(this).hasClass('active') ) {
					if($(this).hasClass('li01')) {flg0204 = 1;}
					else if($(this).hasClass('li02')) {flg0204 = 2;}
					else if($(this).hasClass('li03')) {flg0204 = 3;}
					else {flg0204 = 4;}
				}
			});
			if($('.lesson2 #sec04 .block03 .btn').hasClass('active')) {
				$(result);
			}
		}

		if($(this).closest('.block').hasClass('block01')) {
			$(this).closest('.block').siblings('.block').removeClass('active').hide();
			$(this).closest('.block').siblings('.block03').removeClass('active').hide();
			$(this).closest('.block').siblings('.block').find('li').removeClass('active');
			$(this).closest('.block').siblings('.block').find('img').each(function() {
				$(this).attr("src",$(this).attr("src").replace('_on', '_off'));
			});
			$('.lesson2 #sec04 .result_area').hide().removeClass('active');
			$('.lesson2 #sec04 .block03 .btn').removeClass('active').removeClass('show');
			if( $(this).hasClass('li01') ) {
				$('.lesson2 #sec04 .block01_02').fadeIn();
				flg0202 =0;flg0203=0;flg0204=0;
			}
			else {
				$('.lesson2 #sec04 .block02_02').fadeIn();
				flg0102 =0;flg0103=0;flg0104=0;
			}
		} else if($(this).closest('.block').hasClass('end')) {
			if(!($('.lesson2 #sec04 .block03 .btn').hasClass('show'))) {
				$(this).closest('.block').siblings('.block03').fadeIn().addClass('active');
			}
		}
		 else {
			if( !$(this).closest('.block').next('.block').hasClass('active') ) {
				$(this).closest('.block').next('.block').fadeIn().addClass('active');
			}
		}
	});
	
	$('.lesson2 #sec04 .block03 .btn').click(function() {
		if(!flg2b) {
			localStorage.setItem(c2b,1);
		}
		if( !$(this).hasClass('active')  ) {
			$(this).closest('.block03').hide();
			$(this).addClass('active').addClass('show');
		}
		$(result);
	});
	
	function result() {
		if( !($('.lesson2 #sec04 .result_area').hasClass('active')) ) {
			$('.lesson2 #sec04 .result_area').fadeIn().addClass('active');
		}
		$('.lesson2 #sec04 .result').hide();
		/*赤ワイン*/
		if(flg0101 === 1) {
			if(flg0102 === 1 ) {
				if(flg0103 === 1) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_1').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_1').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_1').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_1').fadeIn();}
				}
				else if(flg0103 === 2) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_1').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_2').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_3').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_5').fadeIn();}
				}
				else if(flg0103 === 3) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_1').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_3').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_3').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_3').fadeIn();}
				}
				else if(flg0103 === 4) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_1').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_3').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_4').fadeIn();}
				}
			}
			else if(flg0102 === 2) {
				if(flg0103 === 1) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_1').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_2').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_5').fadeIn();}
				}
				else if(flg0103 === 2) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_2').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_2').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_2').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_2').fadeIn();}
				}
				else if(flg0103 === 3) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_2').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_3').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_5').fadeIn();}
				}
				else if(flg0103 === 4) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_2').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_4').fadeIn();}
				}
			}
			else if(flg0102 === 3) {
				if(flg0103 === 1) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_1').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_4').fadeIn();}
				}
				else if(flg0103 === 2) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_2').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_4').fadeIn();}
				}
				else if(flg0103 === 3) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_5').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_3').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_4').fadeIn();}
				}
				else if(flg0103 === 4) {
					if(flg0104 === 1) {$('.lesson2 #sec04 .result1_4').fadeIn();}
					else if(flg0104 === 2) {$('.lesson2 #sec04 .result1_4').fadeIn();}
					else if(flg0104 === 3) {$('.lesson2 #sec04 .result1_4').fadeIn();}
					else if(flg0104 === 4) {$('.lesson2 #sec04 .result1_4').fadeIn();}
				}
			}
		}
		/*白ワイン*/
		else if(flg0101 === 2) {
			if(flg0202 === 1 ) {
				if(flg0203 === 1) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_1').fadeIn();}
				}
				else if(flg0203 === 2) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_5').fadeIn();}
				}
				else if(flg0203 === 3) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_5').fadeIn();}
				}
				else if(flg0203 === 4) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_4').fadeIn();}
				}
			}
			else if(flg0202 === 2) {
				if(flg0203 === 1) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_5').fadeIn();}
				}
				else if(flg0203 === 2) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_2').fadeIn();}
				}
				else if(flg0203 === 3) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_5').fadeIn();}
				}
				else if(flg0203 === 4) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_4').fadeIn();}
				}
			}
			else if(flg0202 === 3) {
				if(flg0203 === 1) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_5').fadeIn();}
				}
				else if(flg0203 === 2) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_5').fadeIn();}
				}
				else if(flg0203 === 3) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_3').fadeIn();}
				}
				else if(flg0203 === 4) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_4').fadeIn();}
				}
			}
			else if(flg0202 === 4) {
				if(flg0203 === 1) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_1').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_4').fadeIn();}
				}
				else if(flg0203 === 2) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_2').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_4').fadeIn();}
				}
				else if(flg0203 === 3) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_5').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_3').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_4').fadeIn();}
				}
				else if(flg0203 === 4) {
					if(flg0204 === 1) {$('.lesson2 #sec04 .result2_4').fadeIn();}
					else if(flg0204 === 2) {$('.lesson2 #sec04 .result2_4').fadeIn();}
					else if(flg0204 === 3) {$('.lesson2 #sec04 .result2_4').fadeIn();}
					else if(flg0204 === 4) {$('.lesson2 #sec04 .result2_4').fadeIn();}
				}
			}
		}
	}
	
});



/*lesson3*/
$(function() {
	var flg01,flg02,flg03,flg04,flg05;
	$('.lesson3 #sec04 .td_check').click(function() {
		$(this).addClass('active');
		$(this).siblings('.td_check').removeClass('active');
		if($('.lesson3 #sec04 .td_check.check01').hasClass('active') && $('.lesson3 #sec04 .td_check.check02').hasClass('active') && $('.lesson3 #sec04 .td_check.check03').hasClass('active') && $('.lesson3 #sec04 .td_check.check04').hasClass('active') && $('.lesson3 #sec04 .td_check.check05').hasClass('active')) {
			$('.lesson3 #sec04 .check_btn').addClass('active');
			$('.lesson3 #sec04 .check_btn img').attr("src",$('.lesson3 #sec04 .check_btn img').attr("src").replace('_off', '_on'));
		}
	});

	$('.lesson3 #sec04 .check_btn').click(function() {
		if($(this).hasClass('active')) {
			/*check*/
			if($('.lesson3 #sec04 .td_check.check01.active').hasClass('check0101')) {
				$('.lesson3 #sec04 .td_txt01').removeClass('incorrect').addClass('correct');
				$('.lesson3 #sec04 .td_check.check01').css({'background-color':'#ddf9e0'});
				flg01=1;
			} else {
				$('.lesson3 #sec04 .td_txt01').removeClass('correct').addClass('incorrect');
				$('.lesson3 #sec04 .td_check.check01').css({'background-color':'#fee5e5'});
				flg01=0;
			}
			if($('.lesson3 #sec04 .td_check.check02.active').hasClass('check0202')) {
				$('.lesson3 #sec04 .td_txt02').removeClass('incorrect').addClass('correct');
				$('.lesson3 #sec04 .td_check.check02').css({'background-color':'#ddf9e0'});
				flg02=1;
			} else {
				$('.lesson3 #sec04 .td_txt02').removeClass('correct').addClass('incorrect');
				$('.lesson3 #sec04 .td_check.check02').css({'background-color':'#fee5e5'});
				flg02=0;
			}
			if($('.lesson3 #sec04 .td_check.check03.active').hasClass('check0303')) {
				$('.lesson3 #sec04 .td_txt03').removeClass('incorrect').addClass('correct');
				$('.lesson3 #sec04 .td_check.check03').css({'background-color':'#ddf9e0'});
				flg03=1;
			} else {
				$('.lesson3 #sec04 .td_txt03').removeClass('correct').addClass('incorrect');
				$('.lesson3 #sec04 .td_check.check03').css({'background-color':'#fee5e5'});
				flg03=0;
			}
			if($('.lesson3 #sec04 .td_check.check04.active').hasClass('check0404')) {
				$('.lesson3 #sec04 .td_txt04').removeClass('incorrect').addClass('correct');
				$('.lesson3 #sec04 .td_check.check04').css({'background-color':'#ddf9e0'});
				flg04=1;
			} else {
				$('.lesson3 #sec04 .td_txt04').removeClass('correct').addClass('incorrect');
				$('.lesson3 #sec04 .td_check.check04').css({'background-color':'#fee5e5'});
				flg04=0;
			}
			if($('.lesson3 #sec04 .td_check.check05.active').hasClass('check0505')) {
				$('.lesson3 #sec04 .td_txt05').removeClass('incorrect').addClass('correct');
				$('.lesson3 #sec04 .td_check.check05').css({'background-color':'#ddf9e0'});
				flg05=1;
			} else {
				$('.lesson3 #sec04 .td_txt05').removeClass('correct').addClass('incorrect');
				$('.lesson3 #sec04 .td_check.check05').css({'background-color':'#fee5e5'});
				flg05=0;
			}
			/*result*/
			if(!($('.lesson3 #sec04 .result_area').hasClass('active'))) {
				$('.lesson3 #sec04 .result_area').fadeIn().addClass('active');
				localStorage.setItem('sp_autumn2016_3_b',1);
			}
			$('.lesson3 #sec04 .result_area p').hide();
			if(flg01&&flg02&&flg03&&flg04&&flg05) {
				$('.lesson3 #sec04 .result_area .result_correct').fadeIn();
			} else {
				$('.lesson3 #sec04 .result_area .result_incorrect').fadeIn();
			}
			
		}
	});
});