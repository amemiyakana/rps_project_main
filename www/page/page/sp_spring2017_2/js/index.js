/*＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
common
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝*/
/*resize*/
$(function() {
	var w = $('body').width();
	var x = 950;
	resize();
	$(window).resize(function() {
		w = $('body').width();
		resize();
	});
	function resize() {
		if(w<=x) {
			$('#main').css('width',950);
		} else {
			$('#main').css({'width':''});
		}
	}
});



/*＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
local storage
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝*/
$(function(){
var dir = 'sp_spring2017_';
var flg,num;

	$(window).load(function() {

		/*part1*/
		$('#main.part1 #main_sec02 ul li').each(function(){
			num = dir + $(this).attr('data-num');
			flg = localStorage.getItem(num);
			if(flg) {
				$(this).addClass('active');
			}
		});

		/*entry*/
		$('#main.entry #main_sec02 #ul_part .li_part ul li').each(function(){
			num = dir + $(this).attr('data-num');
			flg = localStorage.getItem(num);
			if(flg) {
				$(this).addClass('active');
			}
		});
		if(
			$('#main.entry #main_sec02 #ul_part #li_part1 ul li.li01').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part1 ul li.li02').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part1 ul li.li03').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part1 ul li.li04').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part1 ul li.li05').hasClass('active') 
		){
			$('#main.entry #main_sec02 #ul_part #li_part1').addClass('active');
		}
		if(
			$('#main.entry #main_sec02 #ul_part #li_part2 ul li.li01').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part2 ul li.li02').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part2 ul li.li03').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part2 ul li.li04').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part2 ul li.li05').hasClass('active') 
		){
			$('#main.entry #main_sec02 #ul_part #li_part2').addClass('active');
		}
		if(
			$('#main.entry #main_sec02 #ul_part #li_part3 ul li.li01').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part3 ul li.li02').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part3 ul li.li03').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part3 ul li.li04').hasClass('active') &&
			$('#main.entry #main_sec02 #ul_part #li_part3 ul li.li05').hasClass('active') 
		){
			$('#main.entry #main_sec02 #ul_part #li_part3').addClass('active');
		}

	});
	
	
});