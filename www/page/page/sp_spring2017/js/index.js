/*＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
common
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝*/
/*resize*/
$(function() {
	var w = $('body').width();
	var x = 950;
	resize();
	$(window).resize(function() {
		w = $('body').width();
		resize();
	});
	function resize() {
		if(w<=x) {
			$('#main').css('width',950);
		} else {
			$('#main').css({'width':''});
		}
	}
});



/*＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
pre
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝*/
/*resize*/
$(function() {
	var w = $('body').width();
	var x = 1100;
	preResize();
	$(window).resize(function() {
		w = $('body').width();
		preResize();
	});
	function preResize() {
		if(w<=x) {
			$('.pre #sec03 .navi li.li_prev').css({'left':'0','margin-left':'0'});
			$('.pre #sec03 .navi li.li_next').css({'right':'0','margin-right':'0'});
		} else {
			$('.pre #sec03 .navi li.li_prev').css({'left':'','margin-left':''});
			$('.pre #sec03 .navi li.li_next').css({'right':'','margin-right':''});
		}
	}
});

/*slider*/
$(function() {
	var width = 990;
	var time = 500;
	var flg,pagerNum;
	$('.pre #sec03 .slider li').append('<div class="mask"></div>');
	$('.pre #sec03 .slider li.active .mask').hide();
	/*slider_navi*/
	$('.pre #sec03 .navi li').click(function(){
		if(!flg) {
			flg=1;
			if($(this).hasClass('li_next')) {
				$('.pre #sec03 .slider li.active').next('li').addClass('move');
				$('.pre #sec03 .slider').not(':animated').animate({'margin-left':'-='+width+'px'},time);
				$('.pre #sec03 .pager li.active').next('li').addClass('move');
			}
			else if($(this).hasClass('li_prev')) {
				$('.pre #sec03 .slider li.active').prev('li').addClass('move');
				$('.pre #sec03 .slider').not(':animated').animate({'margin-left':'+='+width+'px'},time);
				$('.pre #sec03 .pager li.active').prev('li').addClass('move');
			}
			slide();
		}
	});
	/*slider_pager*/
	$('.pre #sec03 .pager li').click(function(){
		if(!flg) {
			if( !$(this).hasClass('active') && !$(this).hasClass('comingsoon')) {
				flg=1;
				$(this).addClass('move');
				pagerNum = $('.pre #sec03 .pager li').index(this);
				$('.pre #sec03 .slider').not(':animated').animate({'margin-left':-width*pagerNum-5},time);
				$('.pre #sec03 .slider li').eq(pagerNum).addClass('move');
				slide();
			}
		}
	});
	/*slider_function*/
	function slide() {
		if ( $('.pre #sec03 .slider li.move').is(':first-child') ) {
			$('.pre #sec03 .navi li.li_prev').hide();
			$('.pre #sec03 .navi li.li_next').show();
		}
		else if ( $('.pre #sec03 .slider li.move').is(':last-child') ) {
			$('.pre #sec03 .navi li.li_prev').show();
			$('.pre #sec03 .navi li.li_next').hide();
		}
		else {
			$('.pre #sec03 .navi li.li_prev').show();
			$('.pre #sec03 .navi li.li_next').show();
		}
		if(navigator.userAgent.indexOf("MSIE") != -1) {
			$('.pre #sec03 .slider li.active .mask').show();
			$('.pre #sec03 .slider li.move .mask').hide();
		}
		else {
			$('.pre #sec03 .slider li.active .mask').fadeIn();
			$('.pre #sec03 .slider li.move .mask').fadeOut();
		}
		setTimeout(function() {
			$('.pre #sec03 .pager li.active').removeClass('active');
			$('.pre #sec03 .pager li.move').removeClass('move').addClass('active');
			$('.pre #sec03 .slider li.active').removeClass('active');
			$('.pre #sec03 .slider li.move').removeClass('move').addClass('active');
		},10);
		setTimeout(function() {
			flg=0;
		},time);
	}
});