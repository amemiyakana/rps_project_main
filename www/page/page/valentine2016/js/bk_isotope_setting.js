var isMoreShow = false;
var itemListIs = false;
var category = "all";
$(function(){
	setIsotope();	
	
	$('.element a').click(function(){
		if(w>=768) {	//*** FOR_PC***
			var $this = $(this);
			// back out if hidden item
			if ( $this.parents('.isotope-item').hasClass('isotope-hidden') ) {
				return;
			}
			
			popUp($(this).attr("href"),$(this).attr("data-width"),$(this).attr("data-height"));
			
			return false;
		}
		else if(0>w && w<=767) {	// ***FOR_MOBILE
			return;
		}
	});
	
	$("#popup_close").click(function(){
		$("#overlay_bg").hide();
		$("#overlay").hide();
		$("#popup").html("");
	});

	$(".no_more").click(function(){
		if(!isMoreShow){
			$("#hist_listArea_btn01").hide();
		}else{
			$("#hist_listArea_btn02").hide();
		}
	});
	$(".yes_more").click(function(){
		if(!isMoreShow){
			$("#hist_listArea_btn01").show();
		}else{
			$("#hist_listArea_btn02").show();
		}
	});

});


function popUp(src,width,height){
		
	$("#popup").css("width",width);
	$("#popup").css("height",parseInt(height));
	$("#popup").html('<iframe id="popup_iframe" frameborder="0"></iframe>');
	$("#popup_close").css("width", width);
	$("#popup_iframe").attr("width",width);
	$("#popup_iframe").attr("height",height);
	$("#popup_iframe").attr("src",src);
	
	var dh = $(document).height();
	var st = $(this).scrollTop();
	$("#overlay_bg").css("height",dh);
	$("#overlay").css("height",dh)
	
	if(st < 0) { top = 50; }
	
	$("#popup").css("top",st+50);
	$("#popup_close").css("top", st+50+10);
	$("#popup_close").css("margin-right", ((width/2)-18)*-1);

	
	$("#overlay_bg").show();
	$("#overlay").show();
	
}

function setIsotope(){
	var $container = $('#hist_listWrap');
	var $area = $("#item_listWrap");
	// initialize isotope
	$container.isotope({
	  itemSelector : '.element',
	  filter : '.close_all'
	});
	$area.isotope({
	  itemSelector : '.element',
	  filter : '.close_item'
	});
	
	// filter items when filter link is clicked
	$('#hist_listFilters a').click(function(){
	  var $filterArea = $('#hist_listWrap');
	  var selector = $(this).attr('data-filter');
		category = selector;
	  var selectId = $(this).attr('id');
	  
		if(isMoreShow){
			selector = "."+selector;
		}else{
			selector = ".close_"+selector;
		}
	  $filterArea.isotope({ filter: selector });
	  
	  $("#hist_listFilters a").removeClass("show").addClass("hidden");
	  $(this).addClass("show");
	  return false;
	});
	
	// filter items when filter link is clicked
	$('#hist_listBtnArea p').click(function(){
		var selector = "";
		if(isMoreShow){
			selector = ".close_"+category;
		}else{
			selector = "."+category;
		}
		$container.isotope({ filter: selector });
		
		if(isMoreShow){
			$("#hist_listArea_btn01").show();
			$("#hist_listArea_btn02").hide();
			isMoreShow = false;
		}else{
			$("#hist_listArea_btn01").hide();
			$("#hist_listArea_btn02").show();
			isMoreShow = true;
		}
		
		return false;
	});
	
	$('#item_listBtnArea p').click(function(){
		var selector = "";
		if(itemListIs){
			selector = ".close_item";
		}else{
			selector = ".item";
		}
		$area.isotope({ filter: selector });
		
		if(itemListIs){
			$("#item_listArea_btn01").show();
			$("#item_listArea_btn02").hide();
			itemListIs = false;
		}else{
			$("#item_listArea_btn01").hide();
			$("#item_listArea_btn02").show();
			itemListIs = true;
		}
		
		return false;
	});
}
