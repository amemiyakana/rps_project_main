$(function(){
	
	var xml_contents;	//contents.xml格納用
	var xml_link;		//link.xml格納用
	
	//xmlから読み込む値
	var imgPrefix;		//画像ディレクトリまでのパス
	var opSpread;		//右開き・左開き
	var totalPages;		//総ページ数（1から数えた数）
	var bookW;			//1ページの幅
	var bookH;			//1ページの高さ
	var sliceW;			//スライス画像横幅
	var sliceH;			//スライス画像縦幅
	var pageStyle;		//ページスタイル（3:複数ページ用(最初の見開きが1ページ)、4:複数ページ用(最初の見開きが2ページ)）
	var nombre;			//ページ数を数える場合の開始数
	var minusPage;		//マイナスページを表示するか（0:表示しない、1:表示する）
	var hidePages;		//非表示ページ（配列に変換）
	
	//設定用変数
	var imgSize=4;			//使用画像（1,2,4）
	var hideZero=true;		//pageStyle=4 のとき、0ページ目の表示（true:表示しない、false:表示する）
	var dblClickTime=300;	//ダブルクリック判定間隔（ms）
	var zoomRatio=2;		//ズーム１回分の拡大率
	var maxZoomRatio=4;		//初期表示に対する最大倍率
	var borderW=5;			//ページ境界の線の幅
	var fixHeader=true;		//ヘッダー固定表示
	
	//汎用変数
	var showPageNum;	//１度に表示するページ数（1/2）
	var initZoom;		//初期ズーム値（最小値）
	var maxZoom;		//最大ズーム値
	var curZoom;		//現在のズーム値
	var curPage;		//現在のページ
	var prevPage;		//前のページのページ番号
	var nextPage;		//次のページのページ番号
	var sliceNumW;		//スライス画像の枚数（横）
	var sliceNumH;		//スライス画像の枚数（縦）
	var windowW;		//ウインドウ横幅
	var windowH;		//ウインドウ高さ
	var isLeftEnd;		//左端ページ
	var isRightEnd;		//右端ページ
	var linkRgba1;		//リンクエリア内の色
	var linkRgba2;		//リンクエリア枠線の色
	var startPage=0;	//表示開始ページ
	var headerH=44;		//ヘッダー高さ
	
	var ua = navigator.userAgent;
	var isIOS = (ua.indexOf('iPhone')!=-1 || ua.indexOf('iPad')!=-1 || ua.indexOf('iPod')!=-1)? true:false;
	var isAndroid = (ua.indexOf('Android')!=-1)? true:false;
	var isTablet = (ua.indexOf('iPad')!=-1 || isAndroid && ua.indexOf('Mobile')==-1)? true:false;
	var isLowSpec = (ua.match(/Android [1-3]/) || isIOS && ua.match(/OS [2-4]/))? true:false;
	var isChrome = (ua.indexOf('Chrome')!=-1)? true:false;
	
	var startEvent;
	var moveEvent;
	var endEvent;
	
	if (isAndroid || isIOS){
		startEvent = 'touchstart';
		moveEvent = 'touchmove';
		endEvent = 'touchend';
	} else {
		startEvent = 'mousedown';
		moveEvent = 'mousemove';
		endEvent = 'mouseup';
	}
	
	
	/*-------------------------------------------------------------------------
	  xml読み込み
	-------------------------------------------------------------------------*/
	
	$('#Loading').show();
	$.ajax({
		type : 'GET',
		url : 'index/contents.xml',
		dataType : 'xml',
		success : function(data, status){
			xml_contents = data;
			
			$.ajax({
				type : 'GET',
				url : 'index/link.xml',
				dataType : 'xml',
				success : function(data, status){
					xml_link = data;
					init();
				},
				error : function(e,t) {
					init();
				}
			});
		},
		error : function(e,t) {
			alert(t);
		}
	});
	
	
	function init(){
		//ロースペックモード
		if (isLowSpec){
			imgSize = 2;
			maxZoomRatio = 2;
		}
		
		//タブレット時
		if (isTablet){
			$('body').addClass('tablet');
		}
		
		//定数設定
		imgPrefix = $('imgPrefix', xml_contents).text();
		opSpread = $('opSpread', xml_contents).text();
		totalPages = parseInt($('totalPages', xml_contents).text());
		bookW = $('bookW', xml_contents).text();
		bookH = $('bookH', xml_contents).text();
		sliceW = $('sliceW', xml_contents).text();
		sliceH = $('sliceH', xml_contents).text();
		pageStyle = $('pageStyle', xml_contents).text();
		nombre = parseInt($('pageAssign', xml_contents).attr('nombre'));
		minusPage = parseInt($('pageAssign', xml_contents).attr('minusPage'));
		hidePages = $('hidePage', xml_contents).text().split(',');
		for (var i=0; i<hidePages.length; i++) hidePages[i] = parseInt(hidePages[i]);
		if (pageStyle==4 && hideZero) startPage = 1;
		
		sliceNumW = Math.ceil(bookW*imgSize/sliceW);
		sliceNumH = Math.ceil(bookH*imgSize/sliceH);
		
		//リンク色設定
		var lc1 = $('link', xml_contents).attr('color1').replace('0x', '');
		var lc2 = $('link', xml_contents).attr('color2').replace('0x', '');
		var la1 = $('link', xml_contents).attr('alpha1').split(',');
		var la2 = $('link', xml_contents).attr('alpha2').split(',');
		linkRgba1 = 'rgba('+parseInt(lc1.substring(0,2),16)+','
			+parseInt(lc1.substring(2,4),16)+','
			+parseInt(lc1.substring(4,6),16)+','+Number(la1[0]/100)+')';
		linkRgba2 = 'rgba('+parseInt(lc2.substring(0,2),16)+','
			+parseInt(lc2.substring(2,4),16)+','
			+parseInt(lc2.substring(4,6),16)+','+Number(la2[0]/100)+')';
		
		
		//表示するページがない場合
		if (totalPages<=hidePages.length) return;
		
		//パラメータ判定
		var url = location.href;
		if (url.match(/directPage=(-?\d+)/))
			curPage = parseInt(RegExp.$1)-nombre;
		else if (location.hash.match(/^#(-?\d+)/))
			curPage = parseInt(RegExp.$1)-nombre;
		else curPage = 0;
		
		//ページ数設定
		$('#JumpAll').text('/'+(totalPages-1+nombre)+'ページ');
		
		//ページ移動ボタン表示・非表示
		if (!isChrome) $('#PageBtn').hide();
		
		//左右ボーダー設定
		$('#SliderArea li').css('border-right-width', borderW+'px');
		
		//ヘッダー固定表示
		if (fixHeader){
			$('#SliderArea').css('margin-top', headerH+'px')
		}
		
		//スライダー
		$('#PageSliderBar').css(opSpread, '12px');
		
		setSize();
		makeIndex();
	}
	
	
	/*-------------------------------------------------------------------------
	  リサイズ時処理
	-------------------------------------------------------------------------*/
	$(window).load(function(){ $(window).resize(setSize); });
	
	function setSize(){
		//ページジャンプ表示時
//		if($('#JumpAreaBtn').hasClass('open')){
//			var jumpAreaH;
//			jumpAreaH = window.innerHeight;
//			$('#JumpTitle').css('margin-top', (jumpAreaH-200)/2+'px');
//			return;
//		}
		
		//縦長・横長判定（表示枚数変更）
		if ($(window).width()>$(window).height()){
			if (showPageNum==2) return;
			showPageNum = 2;
		} else {
			if (showPageNum==1) return;
			showPageNum = 1;
		}
		
		$('#Wrapper').css('height', '1500px');
		setTimeout(function(){
			scrollTo(0,1);

			setTimeout(function(){
				
				//ページ高さ・幅設定
				windowH = window.innerHeight;
				windowW = window.innerWidth;
				
				//ページにフィットするよう調整
				initZoom = Math.min((windowH-(fixHeader?headerH:0))/(bookH*imgSize), windowW/(bookW*imgSize*showPageNum));
				maxZoom = initZoom*maxZoomRatio;
				
				$('#Wrapper').css('width', windowW+'px').css('height', windowH+'px');
				$('#SliderArea').css('width', (windowW+borderW)*3+'px').css('left', -1*(windowW+borderW)+'px');
				$('#SliderArea li').css('width', windowW+'px');
				$('#SliderArea .pageAreaInner')
					.css('width', bookW*imgSize*showPageNum+'px')
					.css('height', bookH*imgSize+'px');
				if($('#JumpAreaBtn').hasClass('open')){
					$('#JumpArea').css('height', (windowH-headerH)+'px');
				}
				$('#JumpAreaInner').css('height', (windowH-headerH)+'px');
				$('#JumpTitle').css('margin-top', (windowH-200)/2+'px');
				$('#Loading').css('height', windowH+'px');
				$('#LoadingInner').css('margin-top', (windowH-47)/2+'px');
				$('#HelpArea').css({
					height: (windowH-headerH)+'px',
					top: -1*windowH+'px'
				});
				
				zoomReset();
				
				//ページ読み込み
				jumpPage(curPage);
			}, 500);
		}, 500);
	}
	
	//ズーム初期化
	function zoomReset(){
		$('#SliderArea .pageAreaInner').css('zoom', initZoom);
		curZoom = initZoom;

		var pageW = bookW*imgSize*showPageNum*initZoom;
		var pageH = bookH*imgSize*initZoom;
		$('.pageArea')
			.css('left', (windowW-pageW)/2+'px')
			.css('top', (windowH-(fixHeader?headerH:0)-pageH)/2+'px');
	}
	
	
	/*-------------------------------------------------------------------------
	  ページジャンプ
	-------------------------------------------------------------------------*/
	function jumpPage(index, isBack){
		index = parseInt(index, 10);
		if (isNaN(index)) return;
		
		var changeNum = showPageNum;
		if (isBack) changeNum*=-1;
		curPage = index;
		isLeftEnd = false;
		isRightEnd = false;
		if (showPageNum==2 && (pageStyle==3 && curPage%2==0 || pageStyle==4 && curPage%2==1)) curPage--;
		
		$('#SliderArea .pageArea').eq(1).find('.thumbnail').html('');
		$('#SliderArea .pageArea').eq(1).find('.mainImg').html('');
		
		setTimeout(function(){
			zoomReset();
			for(;;){
				var result = loadPage(1, curPage);
				if (result=='success') break;
				else if (result=='end') changeNum = -1*showPageNum;
				curPage += changeNum;
			}
			loadNextPage();
			loadPrevPage();
			pageChange();
		}, 100);
	}
	
	//ページ移動時処理
	function pageChange(){
		//ページ移動ボタンの表示・非表示
		if (isLeftEnd) $('#PageBtnLeft').hide();
		else $('#PageBtnLeft').show();
		if (isRightEnd) $('#PageBtnRight').hide();
		else $('#PageBtnRight').show();
		
		makePageNum(curPage);
		
		var range = (windowW-44)/((showPageNum==1? totalPages-startPage:Math.ceil((totalPages+((pageStyle==3)?1:0))/2))*2-2);
		$('#PageSlider').css(opSpread, range*2*(showPageNum==1? curPage-startPage:Math.floor((curPage-startPage+1)/2))+'px');
		$('#PageSliderBar').css('width', range*2*(showPageNum==1? curPage-startPage:Math.floor((curPage-startPage+1)/2))+'px');
	}
	
	//ページ数表示
	function makePageNum(page){
		var jq_pageNum = $('#PageNum div');
		var left = (page>=startPage)? Math.max(page+nombre, startPage+nombre):'';
		var right = (showPageNum==2 && page<totalPages-1)? page+1+nombre:'';
		
		if (left<=0 && minusPage==0) left = '';
		else if (left<0) left = '('+left+')';
		
		if (right!=='' && right<=0 && minusPage==0) right = '';
		else if (right!=='' && right<0) right = '('+right+')';
		
		jq_pageNum.text(left + (left!=='' && right!=='' ? ' - ':'') + right);
		
		if (jq_pageNum.text()=='') jq_pageNum.hide();
		else jq_pageNum.show();
	}
	
	
	/*-------------------------------------------------------------------------
	  ページ移動スライダ
	-------------------------------------------------------------------------*/
	
	$('#PageSlider').on(startEvent, function(e){
		e.preventDefault();
		e.stopPropagation();
		
		//1ページの場合
		if (isLeftEnd && isRightEnd) return;
		
		var slideMax = windowW-44;
		var jq_slider = $('#PageSlider');
		var jq_bar = $('#PageSliderBar');
		var jq_pageNum = $('#PageNum div');
		
		var range = slideMax/((showPageNum==1? totalPages-startPage:Math.ceil((totalPages+(pageStyle==3?1:0))/2))*2-2);
		
		var newPage = curPage;
		
		var touchX;
		if (window.event && event.touches) touchX = event.touches[0].pageX;
		else touchX = e.pageX;
		
		var moveX = null;
		var initX = parseInt(jq_slider.css(opSpread));
		
		$(document).on(moveEvent, function(e){
			e.preventDefault();
			
			if (window.event && event.touches) moveX = event.touches[0].pageX;
			else moveX = e.pageX;
			
			var newX = Math.max(0, Math.min(slideMax, initX+(moveX-touchX)*(opSpread=='left'?1:-1)));
			jq_slider.css(opSpread, newX+'px');
			jq_bar.css('width', newX+'px');
			
			newPage = startPage+(1-showPageNum)+Math.round(newX/(range*2))*showPageNum;
			makePageNum(newPage);
			
		}).on(endEvent, function(e){
			$(document).off(moveEvent);
			$(document).off(endEvent);
			if (newPage!=curPage) jumpPage(newPage);
			else pageChange();
		});
	});
	
	
	/*-------------------------------------------------------------------------
	  ページ読み込み
	-------------------------------------------------------------------------*/
	function loadPrevPage(){
		var index = (opSpread=='left')? 0:2;
		prevPage = curPage-showPageNum;
		for (;;){
			var result = loadPage(index, prevPage);
			if (result=='success') break;
			if (result=='start'){
				if (opSpread=='left') isLeftEnd = true;
				else isRightEnd = true;
				break;
			} else{
				prevPage -= showPageNum;
			}
		}
	}
	
	function loadNextPage(){
		var index = (opSpread=='left')? 2:0;
		nextPage = curPage+showPageNum;
		for(;;){
			var result = loadPage(index, nextPage);
			if (result=='success') break;
			else if (result=='end'){
				if (opSpread=='left') isRightEnd = true;
				else isLeftEnd = true;
				break;
			} else {
				nextPage += showPageNum;
			}
		}
	}
	
	//見開きページを読み込み
	function loadPage(boxNum, pageNum){
		scrollTo(0,1);
		if (showPageNum==2){
			if (opSpread=='left') return loadImage($('#SliderArea .pageArea').eq(boxNum), pageNum, pageNum+1);
			else return loadImage($('#SliderArea .pageArea').eq(boxNum), pageNum+1, pageNum);
		} else {
			return loadImage($('#SliderArea .pageArea').eq(boxNum), pageNum);
		}
	}
	
	//画像読み込み
	function loadImage(jq_page, pageLeft, pageRight){
		var htmlStr='';
		var leftNum, rightNum;
		var imgSrcL, imgSrcR;
		var thumSrc;
		
		jq_page.find('.linkLayer').html('');
		jq_page.css('background-color', '#333');
		
		//冊子のラスト
		if (pageLeft>=totalPages && pageRight==undefined
			|| pageLeft>=totalPages && pageRight!=undefined && pageRight>=totalPages){
				jq_page.find('.thumbnail').html('');
				jq_page.find('.mainImg').html('');
				return 'end';
			}
			
		//すべてhideページの場合
		if ($.inArray(pageLeft, hidePages)!=-1 && ($.inArray(pageRight, hidePages)!=-1 || pageRight==undefined)){
				jq_page.find('.thumbnail').html('');
				jq_page.find('.mainImg').html('');
				return 'hide';
			}
		
		//画像パス生成・サムネイルhtml生成
		if (pageLeft>=startPage && pageLeft<totalPages && $.inArray(pageLeft, hidePages)==-1){
			leftNum = (pageLeft<10? '000': (pageLeft<100? '00': (pageLeft<1000? '0':''))) + pageLeft;
			imgSrcL = imgPrefix+'/'+leftNum+'/';
			
			htmlStr += '<img src="'+imgSrcL+'tmb.jpg" width="'+bookW*imgSize+'" height="'+bookH*imgSize+'">';
			thumSrc = imgSrcL+'tmb.jpg';
			imgSrcL += imgSize+'00_';
		}
		if (pageRight!=undefined && pageRight>=startPage && pageRight<totalPages && $.inArray(pageRight, hidePages)==-1){
			rightNum = (pageRight<10? '000': (pageRight<100? '00': (pageRight<1000? '0':''))) + pageRight;
			imgSrcR = imgPrefix+'/'+rightNum+'/';
			
			htmlStr += '<img src="'+imgSrcR+'tmb.jpg" width="'+bookW*imgSize+'" height="'+bookH*imgSize+'">';
			thumSrc = imgSrcR+'tmb.jpg';
			imgSrcR += imgSize+'00_';
		}
		
		//冊子の先頭
		if (!imgSrcL && !imgSrcR){
			jq_page.find('.thumbnail').html('');
			jq_page.find('.mainImg').html('');
			return 'start';
		}
		
		//hideページがある場合・背景を白に
		if ($.inArray(pageLeft, hidePages)!=-1 || $.inArray(pageRight, hidePages)!=-1){
			jq_page.css('background-color', '#fff');
		}
		
		//右寄せ・左寄せ
		if (pageRight!=undefined){
			if (imgSrcL && !imgSrcR){
				jq_page.css('text-align', 'left');
				jq_page.find('.mainImg').css('left', 0).css('right', 'auto');
			} else if (!imgSrcL && imgSrcR){
				jq_page.css('text-align', 'right');
				jq_page.find('.mainImg').css('left', 'auto').css('right', 0);
			}
		}
		
		//サムネイルを先に表示
		var thumImg = new Image();
		var setting = false;
		$(thumImg).bind('load', function(){
			if (!setting){
				setting = true;
				$('#Loading').hide();
				makeImgHtml();
			}
		});
		thumImg.src = thumSrc;
		jq_page.find('.thumbnail').html(htmlStr);
		
		function makeImgHtml(){
			//html生成
			htmlStr='';
			var edgePixW = bookW*imgSize%sliceW==0? sliceW:bookW*imgSize%sliceW;
			var edgePixH = bookH*imgSize%sliceH==0? sliceH:bookH*imgSize%sliceH;
			for (var i=0; i<sliceNumH; i++){
				if (imgSrcL)
					for (var j=i*sliceNumW; j<(i+1)*sliceNumW; j++){
						var w = ((j+1)%sliceNumW==0)? edgePixW:sliceW;
						var h = (i==sliceNumH-1)? edgePixH:sliceH;
						htmlStr += '<img src="'+imgSrcL+j+'.jpg" width="'+w+'" height="'+h+'">';
					}
				if (imgSrcR)
					for (var j=i*sliceNumW; j<(i+1)*sliceNumW; j++){
						var w = ((j+1)%sliceNumW==0)? edgePixW:sliceW;
						var h = (i==sliceNumH-1)? edgePixH:sliceH;
						htmlStr += '<img src="'+imgSrcR+j+'.jpg" width="'+w+'" height="'+h+'">';
					}
				if (i<sliceNumH-1) htmlStr += '<br>';
			}
			jq_page.find('.mainImg').html(htmlStr);
			
			//リンク表示
			if (imgSrcL) setLink(jq_page.find('.linkLayer'), pageLeft);
			if (imgSrcR) setLink(jq_page.find('.linkLayer'), pageRight, true);
		}
		
		return 'success';
	}
	
	
	/*-------------------------------------------------------------------------
	  リンク設定
	-------------------------------------------------------------------------*/
	function setLink(jq_page, page, isRight){
		if (!xml_link) return;
		
		var offset = isRight? bookW*imgSize : 0;
		
		var links = $('link[page="'+page+'"]', xml_link).last().find('item');
		for (var i=0; i<links.length; i++){
			var l = $(links[i]);
			var newDiv = $('<div class="linkArea"></div>');
			newDiv
				.css('top', parseInt(l.attr('y'))*imgSize+'px')
				.css('left', (offset+parseInt(l.attr('x'))*imgSize)+'px')
				.css('width', parseInt(l.attr('w'))*imgSize+'px')
				.css('height', parseInt(l.attr('h'))*imgSize+'px')
				.css('background-color', linkRgba1)
				.css('border-color', linkRgba2);
			if (l.attr('adress')){
				newDiv.attr('data-adress', l.attr('adress'));
			} else {
				newDiv.attr('data-orderNo', l.attr('orderNo'));
			}
			jq_page.append(newDiv);
		}
	}
	
	
	/*-------------------------------------------------------------------------
	  タッチイベント
	-------------------------------------------------------------------------*/
	var swipeWidth = 50;
	
	var touchX, touchY;		//タッチ開始位置
	var moveX, moveY;		//移動後の指位置
	var scrollX, scrollY;	//ページスクロール初期位置
	var pinchX, pinchY;		//ピンチ中心座標
	var pinchVal;			//ピンチ時の指の間隔
	var isPinch=false;		//ピンチ中
	var isSwipe=false;		//スワイプ中
	var pinchCancel=false;	//ピンチ処理キャンセル
	var clickTimer=null;	//ダブルクリック判定用タイマー保持
	var tapLink;			//リンク
	var moveTimerID;		//タップ判定用タイマー
	var moveFlg;
	
	$('#SliderArea').bind(startEvent, function(e){
		e.preventDefault();
		
		pinchCancel = false;
		if (isSwipe) return;
		
		if ($(e.target).hasClass('linkArea')){
			tapLink = e.target;
		} else {
			tapLink = null;
		}
		
		if (event.touches) {
			var t=event.touches;
			if (!isSwipe && t.length>=2){
				pinchVal = Math.sqrt(Math.pow(t[1].pageX-t[0].pageX, 2) + Math.pow(t[1].pageY-t[0].pageY, 2));
				isPinch = true;
			} else {
				touchX = t[0].pageX;
				touchY = t[0].pageY;
				isPinch = false;
			}
		} else {
			touchX = e.pageX;
			touchY = e.pageY;
			isPinch = false;
		}
		
		moveX = null;
		moveY = null;
		moveFlg = false;
		
		scrollX = parseInt($('#SliderArea .pageArea').eq(1).css('left'));
		scrollY = parseInt($('#SliderArea .pageArea').eq(1).css('top'));
		
		$(document).unbind(moveEvent).unbind(endEvent);
		$(document).bind(endEvent, touchEnd);
		moveTimerID = setTimeout(function(){
			$(document).bind(moveEvent, touchMove);
		}, 100);
	});
	
	function touchMove(e){
		e.preventDefault();
		
		if (event.touches) {
			var t=event.touches;
			if (!isSwipe && t.length>=2){
				var newPinchVal, newZoom;
				pinchX = (t[0].pageX + t[1].pageX)/2;
				pinchY = (t[0].pageY + t[1].pageY)/2;
				newPinchVal = Math.sqrt(Math.pow(t[1].pageX-t[0].pageX, 2) + Math.pow(t[1].pageY-t[0].pageY, 2));
				zoomIn(curZoom*(newPinchVal/pinchVal), pinchX, pinchY);
				pinchVal = newPinchVal;
			} else {
				moveX = t[0].pageX;
				moveY = t[0].pageY;
			}
		} else {
			moveX = e.pageX;
			moveY = e.pageY;
		}
		
		if (touchX != moveX) moveFlg = true;
		
		if (isPinch){
			return;
		} else if (curZoom==initZoom){
			//全体表示の場合：スワイプ
			$('#SliderArea').css('left', -1*windowW+moveX-touchX+'px');
			isSwipe = true;
		} else {
			//拡大表示の場合：ドラッグ
			var newX = scrollX+moveX-touchX;
			var newY = scrollY+moveY-touchY;
			
			if (newX > windowW/2){
				newX = windowW/2;
			} else if (newX < -1*bookW*imgSize*showPageNum*curZoom+windowW/2){
				newX = -1*bookW*imgSize*showPageNum*curZoom+windowW/2;
			}

			if (newY > (windowH-(fixHeader?headerH:0))/2){
				newY = (windowH-(fixHeader?headerH:0))/2;
			} else if (newY < -1*bookH*imgSize*curZoom+(windowH-(fixHeader?headerH:0))/2){
				newY = -1*bookH*imgSize*curZoom+(windowH-(fixHeader?headerH:0))/2;
			}
			
			$('#SliderArea .pageArea').eq(1)
				.css('left', newX+'px')
				.css('top', newY+'px');
		}
	}
	
	function touchEnd(e){
		$(document).unbind(moveEvent).unbind(endEvent);
		clearTimeout(moveTimerID);
		
		if (isPinch) return;
		
		//リンクをタップ
		if (tapLink && !moveFlg && $(tapLink).attr('data-adress')){
			currentPage = curPage;
			if ($(tapLink).attr('data-adress').match(/page:(.*)/)){
				var page = parseInt(RegExp.$1);
				$('#Loading').show();
				jumpPage(page);
				$('#Loading').hide();
//			} else if ($(tapLink).attr('data-adress').match(/javascript:openCart\('(.*)'\)/)){
//				var url = RegExp.$1;
//				openCart(url);
			} else if ($(tapLink).attr('data-adress').match(/javascript:/)){
				eval($(tapLink).attr('data-adress'));
			} else {
				location.href = $(tapLink).attr('data-adress');
			}
			return;
		}
		
		if (!moveFlg){
			if (clickTimer){
				//ダブルタップ
				clearTimeout(clickTimer);
				clickTimer=null;
				if (curZoom<maxZoom){
					zoomIn(curZoom*zoomRatio, touchX, touchY);
				} else {
					zoomReset();
				}
			} else {
				//シングルタップ
				clickTimer = setTimeout(function(){
					if (!fixHeader) $('header').toggle();
					$('#BtmArea').toggle();
					clickTimer=null;
				}, dblClickTime);
			}
		} else 	if (isSwipe && moveX-touchX > swipeWidth && !isLeftEnd){
			//スワイプ（右）
			$('#SliderArea').addClass('move').css('left', 0);
			setTimeout(function(){
				$('#SliderArea li').eq(0).before($('#SliderArea li').eq(2));
				$('#SliderArea').removeClass('move').css('left', -1*(windowW+borderW)+'px');
				isRightEnd = false;
				if (opSpread=='left'){
					nextPage = curPage;
					curPage = prevPage;
					loadPrevPage();
				} else {
					prevPage = curPage;
					curPage = nextPage;
					loadNextPage();
				}
				pageChange();
				shareLinkChange();
				isSwipe = false;
			}, 500);
		} else if (isSwipe && touchX-moveX > swipeWidth && !isRightEnd){
			//スワイプ（左）
			$('#SliderArea').addClass('move').css('left', -2*(windowW+borderW)+'px');
			setTimeout(function(){
				$('#SliderArea li').eq(2).after($('#SliderArea li').eq(0));
				$('#SliderArea').removeClass('move').css('left', -1*(windowW+borderW)+'px');
				isLeftEnd = false;
				if (opSpread=='left'){
					prevPage = curPage;
					curPage = nextPage;
					loadNextPage();
				} else {
					nextPage = curPage;
					curPage = prevPage;
					loadPrevPage();
				}
				pageChange();
				shareLinkChange();
				isSwipe = false;
			}, 500);
		} else if (isSwipe) {
			//スワイプキャンセル
			$('#SliderArea').addClass('move').css('left', -1*(windowW+borderW)+'px');
			setTimeout(function(){
				$('#SliderArea').removeClass('move');
				isSwipe = false;
			}, 500);
		} else {
		}
	}
	
	
	/*-------------------------------------------------------------------------
	  拡大
	-------------------------------------------------------------------------*/
	function zoomIn(newZoom, x, y){
		newZoom = Math.min(newZoom, maxZoom);
		
		if (pinchCancel || curZoom!=initZoom && newZoom<=initZoom){
			$(document).unbind(moveEvent);
			$(document).unbind(endEvent);
			zoomReset();
			pinchCancel = true;
			return;
		}
		
		$('#SliderArea .pageAreaInner').eq(1).css('zoom', newZoom);
		
		scrollX -= (x-scrollX)*(newZoom/curZoom-1);
		scrollY -= (y-scrollY)*(newZoom/curZoom-1);
		
		$('#SliderArea .pageArea').eq(1).css('left', scrollX+'px').css('top', scrollY+'px');
		
		curZoom = newZoom;
	}
	
		
	/*-------------------------------------------------------------------------
	  ヘッダー
	-------------------------------------------------------------------------*/
	
	//メニューボタン
	$('#MenuBtn').bind(startEvent, menuToggle);
	function menuToggle(){
		var menuH = $('#Menu').height();
		if ($('#MenuBtn').hasClass('open')){
			$('#IndexList').animate({ height: '0' }, 500, function(){
				$('#Menu').css('top', 'auto').css('bottom', '0');
			});
			$('#MenuBtn').removeClass('open');
		} else {
			$('#IndexList li.click').removeClass('click');
			if ($('#JumpAreaBtn').hasClass('open')) jumpAreaToggle();
			if ($('#ShareBtn').hasClass('open')) shareToggle();
			if ($('#HelpBtn').hasClass('open')) helpAreaToggle();
			$('#IndexList').animate({ height: (windowH-headerH)+'px' }, 500);
			$('#Menu').animate({ bottom: (windowH-headerH-menuH)+'px' }, 500, function(){
				$('#Menu').css('bottom', 'auto').css('top', '0');
			});
			$('#MenuBtn').addClass('open');
		}
	}
	
	//ページジャンプ表示
	$('#JumpAreaBtn').bind(startEvent, jumpAreaToggle);
	function jumpAreaToggle(){
		if ($('#JumpAreaBtn').hasClass('open')){
			$('#JumpAreaBtn').removeClass('open');
			$('#JumpArea').animate({ height: '0' }, 500);
		} else {
			$('.cmnBtn.click').removeClass('click');
			if ($('#MenuBtn').hasClass('open')) menuToggle();
			if ($('#ShareBtn').hasClass('open')) shareToggle();
			if ($('#HelpBtn').hasClass('open')) helpAreaToggle();
			var defVal = Math.max(curPage+nombre, startPage+nombre);
			if (defVal<0 && minusPage==0) defVal = '';
			if (defVal=='' && curPage+nombre+1==0) defVal = 0;
			$('#JumpPageInput').val(defVal);
			$('#JumpTitle').css('margin-top', (windowH-200)/2+'px');
			$('#JumpArea').animate({ height: (windowH-headerH)+'px' }, 500);
			$('#JumpAreaBtn').addClass('open');
		}
	}
	
	//ページジャンプ 「移動する」ボタン
	$('#JumpBtn').bind(startEvent, function(e){
		e.stopPropagation();
		var page = parseInt($('#JumpPageInput').val(),10)-nombre;
		if (isNaN(page)){
			alert('正しい値を入力して下さい');
		} else {
			$(this).addClass('click');
			$('#JumpPageInput').blur();
			setTimeout(function(){
				jumpAreaToggle();
				setTimeout(function(){
					jumpPage(page);
				}, 1000);
			}, 500);
		}
	});
	
	//ページジャンプ 「＋」ボタン
	$('#JumpPageAdd').bind(startEvent, function(){
		var num = parseInt($('#JumpPageInput').val(),10);
		if (!isNaN(num)){
			$('#JumpPageInput').val(Math.min(num+1, totalPages-1+nombre));
		} else {
			$('#JumpPageInput').val(startPage);
		}
	});
	
	//ページジャンプ 「－」ボタン
	$('#JumpPageDec').bind(startEvent, function(){
		var num = parseInt($('#JumpPageInput').val(),10);
		if (!isNaN(num)){
			var minPage = (minusPage==0)? Math.max(startPage+nombre, 0):startPage+nombre;
			$('#JumpPageInput').val(Math.max(num-1, minPage));
		}
	});
	
	//シェアボタン
	$('#ShareBtn').bind(startEvent, shareToggle);
	function shareToggle(){
		var menuH = $('#ShareList').height();
		if ($('#ShareBtn').hasClass('open')){
			$('#ShareArea').animate({ height: '0' }, 500);
			$('#ShareBtn').removeClass('open');
		} else {
			shareLinkChange();
			if ($('#MenuBtn').hasClass('open')) menuToggle();
			if ($('#JumpAreaBtn').hasClass('open')) jumpAreaToggle();
			if ($('#HelpBtn').hasClass('open')) helpAreaToggle();
			$('#ShareArea').animate({ height: menuH+'px' }, 500);
			$('#ShareBtn').addClass('open');
		}
	}
	
	function shareLinkChange(){
		var url = location.href;
		url = url.replace('sp.html', 'index.html');
		
		if (url.indexOf('directPage')!=-1){
			url = url.replace(/directPage=-?\d*/, 'directPage='+(Math.max(0, curPage)+nombre));
		} else if (url.indexOf('?')!=-1){
			url += '&directPage='+(Math.max(0, curPage)+nombre);
		} else {
			url += '?directPage='+(Math.max(0, curPage)+nombre);
		}
		url = url.replace(/:/g, '%3a')
				.replace(/\//g, '%2f')
				.replace(/\./g, '%2e')
				.replace(/\?/g, '%3f')
				.replace(/=/g, '%3d')
				.replace(/&/g, '%26');
		$('#ShareFacebook a').attr('href', 'http://www.facebook.com/sharer.php?u='+url);
		$('#ShareTwitter a').attr('href', 'https://twitter.com/share?url='+url);
		$('#ShareMail a').attr('href', 'mailto:?body='+url);
	}
	
	//見開きリンクボタン
	$('#PageLinkBtn').bind(startEvent, function(){
		var paramPage = Math.max(0, curPage)+nombre;
		var adjust = (pageStyle==3 && nombre%2==0 || pageStyle==4 && nombre%2!=0)? 1:-1;
		if (paramPage%2!=0 && (adjust!=1 || paramPage!=totalPages-1+nombre)) paramPage += adjust;
		openPageCart(paramPage);
	});
	
	//ヘルプボタン
	$('#HelpBtn').bind(startEvent, helpAreaToggle);
	function helpAreaToggle(){
		if ($('#HelpBtn').hasClass('open')){
			$('#HelpArea').css('top', -1*windowH+'px');
			$('#HelpBtn').removeClass('open');
		} else {
			$('#HelpArea').css('top', headerH+'px');
			$('#HelpBtn').addClass('open');
			
			var scrollH = windowH-96;
			if ($('#HelpScrollInner').height()<scrollH){
				$('#HelpScrollArea').css('height', 'auto');
				$('#HelpScrollBar').hide();
			} else {
				$('#HelpScrollArea').css('height', scrollH+'px').scrollTop(0);
				$('#HelpScrollBar')
					.show()
					.css('top', '35px')
					.css('height', (scrollH-10)*scrollH/$('#HelpScrollInner').height()+'px');
			}
		}
	}
	
	$('#HelpCloseBtn').bind(startEvent, helpAreaToggle);
	
	$('#HelpScrollArea').bind(startEvent, function(e){
		e.stopPropagation();
		
		var jq_scrollArea = $('#HelpScrollArea');
		var jq_scrollBar = $('#HelpScrollBar');
		var scrollMax = $('#HelpScrollInner').height()-jq_scrollArea.height();
		var scrollBarMax = windowH-111-$('#HelpScrollBar').height();
		
		var touchY;
		if (event.touches) touchY = event.touches[0].pageY;
		else touchY = e.pageY;
		
		var moveY = null;
		var initY = jq_scrollArea.scrollTop();
		
		$(document).bind(moveEvent, function(e){
			e.preventDefault();
			
			if (event.touches) moveY = event.touches[0].pageY;
			else moveY = e.pageY;

			jq_scrollArea.scrollTop(initY+touchY-moveY);
			jq_scrollBar.css('top', 35+scrollBarMax*jq_scrollArea.scrollTop()/scrollMax+'px');
			
		}).bind(endEvent, function(e){
			$(document).unbind(moveEvent);
			$(document).unbind(endEvent);
		});
	});
	
	$(window).unload(function(){
		$('.open').removeClass('open');
		$('#Loading').hide();
	});
	
	
	/*-------------------------------------------------------------------------
	  もくじ
	-------------------------------------------------------------------------*/
	//もくじ生成
	function makeIndex(){
		var pageList = $('contents', xml_contents).children();
		
		//もくじがない場合
		if (pageList.length==0){
			$('#MenuBtn').hide();
			return;
		}
		
		function makeIndexSub(ary){
			for (var i=0; i<ary.length; i++){
				if ($(ary[i]).attr('title')){
					var childPageList = $(ary[i]).children();
					htmlStr += '<li class="folder close">'
						+'<a class="folderTit" href="javascript:void(0);"><div>'+$(ary[i]).attr('title')+'</div></a>'
						+'<ul>';
					makeIndexSub(childPageList);
					htmlStr += '</ul>';
				} else {
					htmlStr += listStr($(ary[i]).attr('no'), $(ary[i]).text());
				}
			}
		}
		
		var htmlStr='';
		makeIndexSub(pageList);
		$('#Menu').html(htmlStr);
		
		function listStr(no, txt){
			var cla = (no=='')? ' class="midashi"':'';
			txt = txt.replace(/<\/?font[\s\w\d=#"]*>/g, '');
			return '<li'+cla+'><a href="javascript:void(0);" data-page="'+no+'"><div>'+txt+'</div></a></li>';
		}
		
		//イベント登録
		$('#IndexList li').bind(startEvent, function(e){
			e.stopPropagation();
			
			var _this = this;
			var jq_index = $('#Menu');
			var menuH = jq_index.height();
			
			var touchY;
			if (event.touches) {
				touchY = event.touches[0].pageY;
			} else {
				touchY = e.pageY;
			}
			
			var moveY = null;
			var initY = parseInt(jq_index.css('top'));
			var moveFlg = false;
			
			$(document).bind(moveEvent, function(e){
				e.preventDefault();
				
				if (windowH-headerH>menuH){
					moveY = 0;
					return;
				}
				
				if (event.touches) {
					moveY = event.touches[0].pageY;
				} else {
					moveY = e.pageY;
				}
				
				if (Math.abs(touchY-moveY)>3) moveFlg = true;
				
				var newTop = initY+moveY-touchY;
				if (newTop>0) newTop = 0;
				else if (newTop<windowH-headerH-menuH) newTop = windowH-headerH-menuH;
				jq_index.css('top', newTop+'px');
				
			}).bind(endEvent, function(e){
				$(document).unbind(moveEvent);
				$(document).unbind(endEvent);
				
				if (moveFlg) return;
				
				$(_this).addClass('click');
				
				setTimeout(function(){
					if ($(_this).hasClass('folder')){
						$(_this).removeClass('click');
						if ($(_this).hasClass('close')){
							$(_this).removeClass('close').addClass('open');
						} else {
							$(_this).removeClass('open').addClass('close');
							var menuH = $('#Menu').height();
							var maxT = Math.min(0, windowH-headerH-menuH);
							if (parseInt($('#Menu').css('top'))<maxT){
								$('#Menu').css('top', maxT+'px');
							}
						}
					} else if ($(_this).find('a').data('page')!==''){
						jumpPage($(_this).find('a').data('page'));
						menuToggle();
					}
				}, 100);
				
			});
		});
	}
	
	
	/*-------------------------------------------------------------------------
	  ページ移動ボタン
	-------------------------------------------------------------------------*/
	
	$('#PageBtnLeft').bind(startEvent, function(){
		if (opSpread=='left'){
			jumpPage(curPage-showPageNum, true);
		} else {
			jumpPage(curPage+showPageNum);
		}
	});
	
	$('#PageBtnRight').click(function(){
		if (opSpread=='left') jumpPage(curPage+showPageNum);
		else jumpPage(curPage-showPageNum, true);
	});
	
});


/*-------------------------------------------------------------------------
  スマホ用 openCart
-------------------------------------------------------------------------*/
var currentPage;
function openCart(url){
	var ua = navigator.userAgent;
	if (open_type===0 || ua.match(/Android [1-3]/)){
		if(history.pushState && history.state !== undefined){
			history.replaceState(null, null, '#'+currentPage);
		}else{
			location.replace('#'+currentPage);
		}
		location.href = url;
	} else if (open_type===1){
		if((ua.match(/ iPhone OS (\d+)_/) && RegExp.$1 >= 8)
		|| ua.match(/Android .* Chrome\/.* Mobile/)){
			window.open(url);
		}else{
			var win = window.open(url, 'cart');
			win.focus();
		}
	} else {
		alert('open_typeを指定してください');
	}
}

