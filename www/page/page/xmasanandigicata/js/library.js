function openWin(myURL,myW,myH,myName){
	var scWidthCenter=screen.availWidth/2;
	var scHeightCenter=screen.availHeight/2;
	option="toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,width="+myW+",height="+myH+",left="+(scWidthCenter-(myW/2))+",top="+(scHeightCenter-(myH/2));
	var newWin=window.open(myURL,myName,option);
	newWin.window.focus();
}

function setParam(){
	var nombre = 1;
	var r_array = new Array();
	var r_array2 = new Array();
	var result = "";
	var sea_length = location.search.length;
	var equal_chara = location.search.indexOf("?")+1;
	if(sea_length>1){
		r_array = location.search.substring(equal_chara,sea_length).split("&");
	}
	for(i in r_array){
		r_array2.push(r_array[i].split("="));
		if(r_array2[i][0] == "directPage"){
			r_array2[i][1] -= nombre;
		}
		if (r_array2[i][1] == NaN) {
			result += r_array2[i][0] + "=";
		} else {
			result += r_array2[i][0] + "=" + r_array2[i][1];
		}
		if(r_array.length-1>i){
			result += "&";
		}
	}
	return unescape(result);
}

function quit(){
	window.close();
}

function openCart(URL){
	var win;
	var strUrl=URL;

    try {
      if(parent.opener.closed) {
        win = window.open(strUrl);
        if(win != null) {
          parent.opener = win;
        }
      } else {
        parent.opener.location.href = strUrl;
      } 
    } catch(e) {
      var win = window.open(strUrl);
      if(win != null) {
        parent.opener = win;
      }
    }
    
    try {
      if(parent.opener != null) {
        parent.opener.focus();
      }
    } catch(e) {}
}

function openPageCart(pageNum){
 alert('openPageCart: '+pageNum);
}

function open_pdf(pageNum){
 alert('open_pdf: '+pageNum);
}

function open_pdf_both(pageNum){
 alert('open_pdf_both: '+pageNum);
}