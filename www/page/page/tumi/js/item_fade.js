$(function() {
	
	var item_num = $("#item_list_body .item_list_child").length;
	var item_height = $("#item_list_body .item_list_child").outerHeight()-1;
	
	var colum_num = 4;
	var row_num = Math.ceil(item_num/colum_num);
	
	var colum_blank_num = (item_num%colum_num == 0)? 0 : colum_num-(item_num%colum_num);
	var blank_box = '<div class="item_list_child">&nbsp;</div>';
	
	/*_ DEFAULT SETTING _*/
	$("#item_list_body .item_list_child a img").hide();
	$("#item_list_body").css("height", item_height*row_num + "px");
	
	for (var i=0; i<colum_blank_num; i++) {
		$("#item_list_body").append(blank_box);
	}
		item_num = item_num+colum_blank_num;
		
	/*_ ARRANGEMENT _*/
	var row = 0;
	var column = 0;
	for(var i=0; i<item_num; i++) {
		if(i!=0 && i%colum_num==0) {
			row = row+1;
			column = 0;
		}
		$("#item_list_body .item_list_child").eq(i).css({"top": item_height*row + "px", "left": item_height*column + "px"});
		column++;
	}
	
	/*_ FADE IN ANIMATION _*/
	for ( var i=0; i<item_num; i++ ) {
		$("#item_list_body .item_list_child a").eq(i).children("img").delay(i*230).fadeIn("slow");
	}

});
