$(function() {
	if (jQuery.support.opacity) {
		$('#main').addClass('over');
	}
});

$(function() {
	var flg01 = localStorage.getItem('cp_summer2016_2_01');
	var flg02 = localStorage.getItem('cp_summer2016_2_02');
	var flg03 = localStorage.getItem('cp_summer2016_2_03');
	var flg04 = localStorage.getItem('cp_summer2016_2_04');
	var flg05 = localStorage.getItem('cp_summer2016_2_05');
	var flg06 = localStorage.getItem('cp_summer2016_2_06');
	if(flg01) {
		$('#main #sec02 .cnts01 .ul01 .li01 img').attr('src',
		$('#main #sec02 .cnts01 .ul01 .li01 img').attr('src').replace('_off', '_on'));
		$('#main #sec02 .cnts01 .ul02 .li01').show();
	}
	if(flg02) {
		$('#main #sec02 .cnts01 .ul01 .li02 img').attr('src',
		$('#main #sec02 .cnts01 .ul01 .li02 img').attr('src').replace('_off', '_on'));
		$('#main #sec02 .cnts01 .ul02 .li02').show();
	}
	if(flg03) {
		$('#main #sec02 .cnts01 .ul01 .li03 img').attr('src',
		$('#main #sec02 .cnts01 .ul01 .li03 img').attr('src').replace('_off', '_on'));
		$('#main #sec02 .cnts01 .ul02 .li03').show();
	}
	if(flg04) {
		$('#main #sec02 .cnts01 .ul01 .li04 img').attr('src',
		$('#main #sec02 .cnts01 .ul01 .li04 img').attr('src').replace('_off', '_on'));
		$('#main #sec02 .cnts01 .ul02 .li04').show();
	}
	if(flg05) {
		$('#main #sec02 .cnts01 .ul01 .li05 img').attr('src',
		$('#main #sec02 .cnts01 .ul01 .li05 img').attr('src').replace('_off', '_on'));
		$('#main #sec02 .cnts01 .ul02 .li05').show();
	}
	if(flg06) {
		$('#main #sec02 .cnts01 .ul01 .li06 img').attr('src',
		$('#main #sec02 .cnts01 .ul01 .li06 img').attr('src').replace('_off', '_on'));
		$('#main #sec02 .cnts01 .ul02 .li06').show();
	}
	if(flg01&&flg02&&flg03&&flg04&&flg05&&flg06) {
		$('#main #sec02 .cnts02 .btn').addClass('complete');
	}
});
