<?xml version="1.0" encoding="Shift_JIS"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.0//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja">
<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=Shift_JIS" />
<title>Platinum Salon News 一覧</title>
<style type="text/css"><![CDATA[
body,div,p,dl,dd { margin:0; padding:0; } td { font-size:small; }
a:link,a:focus,a:visited { color:#000000; }
img { border-color:#aaaaaa; }
]]></style>
</head>
<body id="List" class="item" bgcolor="#000000" text="#aaaaaa" style="background:#000000; color:#aaaaaa; font-size:small;">
<div id="pagetop" name="pagetop">

<hr style="width:100%;color:#b7b7b7;background-color:#b7b7b7;border:0px solid #b7b7b7;margin:0em 0;" size="1" />
<div id="h1" style="text-align:center; color:#ffffff;">Platinum Salon News 一覧</div>
<hr style="width:100%;color:#b7b7b7;background-color:#b7b7b7;border:0px solid #b7b7b7;margin:0em 0;" size="1" />

<br />

<div style="text-align:center;">
<div>
	<span style="color:#555555;">前へ</span>
	　　[1/5]　　
	<a href="#"><span style="color:#aaaaaa;">次へ</span></a>
</div>
<span style="color:#555555;">&lt;&lt;</span>
 <span style="color:#555555;">1</span>
 <a href="#"><span style="color:#aaaaaa;">2</span></a>
 <a href="#"><span style="color:#aaaaaa;">3</span></a>
 <a href="#"><span style="color:#aaaaaa;">4</span></a>
 <a href="#"><span style="color:#aaaaaa;">5</span></a>
<span style="color:#aaaaaa;">&gt;&gt;</span>
</div>

<hr style="width:100%;color:#5c5c5c;background-color:#5c5c5c;border:0px solid #5c5c5c;margin:0em 0;" size="1" />
<br />

<div>
<img src="../../upload/info/sample01-00-w75-h57.jpg" style="float:left;margin-bottom:3px;margin-right:3px;margin-bottom:3px;" align="left" />
<span style="font-weight:bold;">2010.07.21</span><br />
<a href="#"><span style="color:#ffffff;">【新発売】〈ｻﾑｿﾅｲﾄ･ﾌﾞﾗｯｸﾚｰﾍﾞﾙ〉新作先行受注会</span></a>
<div clear="all" style="clear:both;font-size:x-small;"></div>
<div>8月1日(日)発売･日本限定｢ｴｱﾛﾋﾟｰｼｰ ﾊﾟｰﾙﾎﾜｲﾄ｣の先行受注会を開催!</div>
<div>
	<span style="color:#b7b263;">■</span>7月23日(金)〜31日(土)<br />
	<span style="color:#b7b263;">■</span>B館1階=ｻﾑｿﾅｲﾄ･ﾌﾞﾗｯｸﾚｰﾍﾞﾙ
</div>
</div>

<br />
<hr style="width:100%;color:#5c5c5c;background-color:#5c5c5c;border:0px solid #5c5c5c;margin:0em 0;" size="1" />
<br />

<div>
<img src="../../upload/info/sample02-00-w75-h57.jpg" style="float:left;margin-bottom:3px;margin-right:3px;margin-bottom:3px;" align="left" />
<span style="font-weight:bold;">2010.07.20</span><br />
<a href="#"><span style="color:#ffffff;">【NEW】ﾃﾞｻﾞｲﾅｰｽﾞ ｽﾍﾟｼｬﾙ ｾｰﾙｸﾚｰﾍﾞﾙ〉新作先行受注会</span></a>
<div clear="all" style="clear:both;font-size:x-small;"></div>
<div>有名人気ﾌﾞﾗﾝﾄﾞを､特別お買得価格でご用意いたします｡</div>
<div>
	<span style="color:#b7b263;">■</span>7月22日(木)〜25日(日)<br />
	<span style="color:#b7b263;">■</span>A館7階=催事場
</div>
</div>

<br />
<hr style="width:100%;color:#5c5c5c;background-color:#5c5c5c;border:0px solid #5c5c5c;margin:0em 0;" size="1" />
<br />

<div>
<img src="../../upload/info/sample03-00-w75-h57.jpg" style="float:left;margin-bottom:3px;margin-right:3px;margin-bottom:3px;" align="left" />
<span style="font-weight:bold;">2010.07.16</span><br />
<a href="#"><span style="color:#ffffff;">【秋の新作先行販売】ｼｰ ﾊﾞｲ ｸﾛｴ 期間限定ﾌﾞﾃｨｯｸ</span></a>
<div clear="all" style="clear:both;font-size:x-small;"></div>
<div>全国8月4日(水)発売に先がけ､秋の新作ﾊﾞｯｸﾞをご紹介いたします｡</div>
<div>
	<span style="color:#b7b263;">■</span>7月20日(火)〜8月2日(月)<br />
	<span style="color:#b7b263;">■</span>A館3階=ﾌﾟﾛﾓｰｼｮﾝｽﾍﾟｰｽ
</div>
</div>

<br />
<hr style="width:100%;color:#5c5c5c;background-color:#5c5c5c;border:0px solid #5c5c5c;margin:0em 0;" size="1" />
<br />

<div>
<img src="../../upload/info/sample04-00-w75-h57.jpg" style="float:left;margin-bottom:3px;margin-right:3px;margin-bottom:3px;" align="left" />
<span style="font-weight:bold;">2010.07.16</span><br />
<a href="#"><span style="color:#ffffff;">ﾎﾞﾃﾞｨｰﾌｨｯﾀｰによるﾌｨｯﾃｨﾝｸﾞｱﾄﾞﾊﾞｲｽﾌｪｱ</span></a>
<div clear="all" style="clear:both;font-size:x-small;"></div>
<div>ｲﾝﾅｰから､私を変える｡専門のﾎﾞﾃﾞｨｰﾌｨｯﾀｰがｼﾞｬｽﾄﾌｨｯﾄのｲﾝﾅｰをｱﾄﾞﾊﾞｲｽ!</div>
<div>
	<span style="color:#b7b263;">■</span>7月29日(木)まで<br />
	<span style="color:#b7b263;">■</span>A館6階=ｲﾝﾅｰｳｴｱ
</div>
</div>

<br />
<hr style="width:100%;color:#5c5c5c;background-color:#5c5c5c;border:0px solid #5c5c5c;margin:0em 0;" size="1" />
<br />

<div>
<img src="../../upload/info/sample05-00-w75-h57.jpg" style="float:left;margin-bottom:3px;margin-right:3px;margin-bottom:3px;" align="left" />
<span style="font-weight:bold;">2010.07.15</span><br />
<a href="#"><span style="color:#ffffff;">【限定】〈Y's〉2010年秋冬ｺﾚｸｼｮﾝ 西武渋谷店 限定ｼｮｯﾌﾟ</span></a>
<div clear="all" style="clear:both;font-size:x-small;"></div>
<div>Y'sの秋冬ｺﾚｸｼｮﾝから､西武渋谷店だけの1点ｺﾚｸｼｮﾝを取りそろえた限定ｼｮｯﾌﾟがｵｰﾌﾟﾝいたしました｡</div>
<div>
	<span style="color:#b7b263;">■</span>7月22日(木)まで<br />
	<span style="color:#b7b263;">■</span>A館4階=ﾌﾟﾛﾓｰｼｮﾝｽﾍﾟｰｽ
</div>
</div>

<br />
<hr style="width:100%;color:#5c5c5c;background-color:#5c5c5c;border:0px solid #5c5c5c;margin:0em 0;" size="1" />

<div style="text-align:center;">
<div>
	<span style="color:#555555;">前へ</span>
	　　[1/5]　　
	<a href="#"><span style="color:#aaaaaa;">次へ</span></a>
</div>
<span style="color:#555555;">&lt;&lt;</span>
 <span style="color:#555555;">1</span>
 <a href="#"><span style="color:#aaaaaa;">2</span></a>
 <a href="#"><span style="color:#aaaaaa;">3</span></a>
 <a href="#"><span style="color:#aaaaaa;">4</span></a>
 <a href="#"><span style="color:#aaaaaa;">5</span></a>
<span style="color:#aaaaaa;">&gt;&gt;</span>
</div>

<hr style="width:100%;color:#5c5c5c;background-color:#5c5c5c;border:0px solid #5c5c5c;margin:0em 0;" size="2" />

<div style="text-align:center;">
<div>---CATEGORY---</div>
<a href="#"><span style="color:#ae3cae;">▼ﾗｸﾞｼﾞｭｱﾘｰ</span></a>
 <a href="#"><span style="color:#e0681c;">▼ｺｽﾒ&amp;ﾋﾞｭｰﾃｨｰ</span></a>
 <a href="#"><span style="color:#6497ee">▼ﾌｧｯｼｮﾝ</span></a>
 <a href="#"><span style="color:#167e7f;">▼雑貨</span></a> 
 <a href="#"><span style="color:#9c0001;">▼ﾌｰﾄﾞ</span></a>
 <a href="#"><span style="color:#afa303;">▼ﾂｱｰ&amp;ｴﾝﾀ-ﾃｲﾝﾒﾝﾄ</span></a>
</div>

<hr style="width:100%;color:#5c5c5c;background-color:#5c5c5c;border:0px solid #5c5c5c;margin:0em 0;" size="3" />

<div>
<a href="#"><span style="color:#ffffff;">&gt;&gt;本ｻｲﾄについて</span></a><br />
<a href="#"><span style="color:#ffffff;">&gt;&gt;ご利用ｶﾞｲﾄﾞ</span></a><br />
<a href="#"><span style="color:#ffffff;">&gt;&gt;履歴一覧</span></a><br />
<a href="#"><span style="color:#ffffff;">&gt;&gt;担当者へのお問い合わせ</span></a><br />
<br />
<a href="#"><span style="color:#ffffff;">&gt;&gt;ﾛｲﾔﾙ･ﾌﾟﾗﾁﾅ ｾﾚｸｼｮﾝTOPへ</span></a>
</div>

<br />

<div style="text-align:center; color:#ffffff; background:#333333;">
(C) Sogo&amp;Seibu
</div>

</div>
</body>
</html>